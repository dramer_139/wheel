package designPattern.singleton;

public enum SingletonEnum {
    INSTANCE;

    private String filed01;

    public String getFiled01() {
        return filed01;
    }
    public void setFiled01(String filed01) {
        this.filed01 = filed01;
    }

    /**
     * 用法
     */
    public static void main(){

        SingletonEnum.INSTANCE.setFiled01("123");

        System.out.println(SingletonEnum.INSTANCE.getFiled01());
    }
}
