package designpattern.templatemethod;

/**
 * @author chencheng
 * @Description
 * @date 2019/10/14
 */
public class ConcreteTemplate extends TemplateAbstract {
    @Override
    protected void abstractMethod() {
        //业务相关代码
    }
    @Override
    public void doMethod(){
        //业务相关代码
    }
}
