package designpattern.templatemethod.demo;

/**
 * @author chencheng
 * @Description
 * @date 2019/10/14
 */
public class MoneyMarketAccount extends Account {
    @Override
    protected String calculateAccountType() {

        return "Money Market";
    }

    @Override
    protected double calculateInterestRate() {

        return 0.045;
    }
}
