package designpattern.templatemethod.demo;

/**
 * @author chencheng
 * @Description
 * @date 2019/10/14
 */
public abstract class Account {
    /**
     * 模板方法
     * @return
     */
    public final double calculateInterest(){
        double interestRate = calculateInterestRate();
        String accountType = calculateAccountType();
        double amount = calculateAmount(accountType);
        return amount * interestRate + doAddExtraFee();
    }

    /**
     * 抽象方法，由子类实现
     * @return
     */
    protected abstract String calculateAccountType();

    /**
     * 抽象方法，由子类实现
     * @return
     */
    protected abstract double calculateInterestRate();

    /**
     * 钩子方法，抽象类给出一个默认实现，由子类选择是否重写
     * @return
     */
    protected double doAddExtraFee(){
        return 0;
    }

    /**
     * 具体方法，子类不重写
     * @param accountType
     * @return
     */
    private double calculateAmount(String accountType){
        /**
         * 省略业务
         */
        return 234.3;
    }
}
