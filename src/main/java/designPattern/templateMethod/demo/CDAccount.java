package designpattern.templatemethod.demo;


/**
 * @author chencheng
 * @Description
 * @date 2019/10/14
 */
public class CDAccount extends Account {
    @Override
    protected String calculateAccountType() {
        return "Certificate of Deposite";
    }

    @Override
    protected double calculateInterestRate() {
        return 0.06;
    }

    @Override
    protected double doAddExtraFee(){
        return 1.0;
    }
}
