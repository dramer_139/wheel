package designpattern.templatemethod.demo;

/**
 * @author chencheng
 * @Description 这里用简单工厂模式来模拟生产客户端，模板模式经常与工厂模式搭配使用，如下这样
 * @date 2019/10/14
 */
public class ClientFactory {
    private CDAccount cdAccount = new CDAccount();
    private MoneyMarketAccount moneyMarketAccount = new MoneyMarketAccount();

    public Account getAccount(int type) {
        switch (type) {
            case 1:
                return cdAccount;
            case 2:
                return moneyMarketAccount;
            default:
                return cdAccount;
        }
    }

    public static void main(String[] args) {
        int type = 1;
        ClientFactory clientFactory = new ClientFactory();
        Account account = clientFactory.getAccount(type);
        System.out.println("利息数额为：" + account.calculateInterest());
    }

}
