package designpattern.templatemethod;

/**
 * @author chencheng
 * @Description 模板方法模式
 * 基于继承的代码复用的基本技术,模板方法所代表的行为称为顶级行为，其逻辑称为顶级逻辑
 * 模版抽象类的模板方法，要求高度抽象，穷举所有步骤，子类只是实现抽象类留给子类的基本抽象方法和空方法
 * 一个模板方法是定义在抽象类中的，把基本操作方法组合在一起形成一个总算法或一个总行为的方法。
 * 一个抽象类可以有任意多个模板方法，而不限于一个。每一个模板方法都可以调用任意多个具体方法。
 * 抽象方法：一个抽象方法由抽象类声明，由具体子类实现。在Java语言里抽象方法以abstract关键字标示。
 * 具体方法：一个具体方法由抽象类声明并实现，而子类并不实现或置换。
 * 钩子方法：一个钩子方法由抽象类声明并实现，而子类会加以扩展。通常抽象类给出的实现是一个空实现，作为方法的默认实现。
 * 钩子方法的名字应当以do开始，这是熟悉设计模式的Java开发人员的标准做法。在上面的例子中，钩子方法hookMethod()应当以do开头；在HttpServlet类中，也遵从这一命名规则，如doGet()、doPost()等方法。
 * TemplateAbstract，它带有三个方法。其中abstractMethod()是一个抽象方法，它由抽象类声明为抽象方法，并由子类实现；
 * hookMethod()是一个钩子方法，它由抽象类声明并提供默认实现，并且由子类置换掉。
 * concreteMethod()是一个具体方法，它由抽象类声明并实现。
 * @date 2019/10/14
 */
public abstract class TemplateAbstract {
    /**
     * 模板方法-调用基本方法
     */
    public final void templateMethod(){
        abstractMethod();
        doMethod();
        concreteMethod();
    }

    /**
     * 基本方法-抽象方法，交给子类去实现
     */
    protected abstract void abstractMethod();

    /**
     * 基本方法-空方法，钩子方法
     */
    protected void doMethod(){}

    /**
     * 基本方法-已经实现的，定义private
     */
    private void concreteMethod(){
        //业务相关代码
    }

}
