package test;

/**
 * Created by ccmac on 2018/6/3.
 */
public class TestBean {
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    private Integer code;
    private Integer timestamp;


}
