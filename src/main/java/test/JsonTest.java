package test;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ccmac on 2018/6/3.
 */
public class JsonTest {
    public static void main(String[] args){
        List<TestBean> list = new ArrayList<TestBean>();
        for (int i=0;i<10;i++){
            TestBean testBean = new TestBean();
            testBean.setCode(i);
            testBean.setTimestamp(1243220000+i);
            list.add(testBean);
        }
        String s = JSON.toJSONString(list);
        List<TestBean> beans = JSON.parseArray(s,TestBean.class);
        System.out.print("");
    }
}
