package util;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author chencheng
 * @Description
 * @date 2019/8/8
 */
public class DateUtils {
    /**
     * 将时间戳转为年月日
     *
     * @param timestamp
     * @return
     */
    public static String timeStampToDateString(long timestamp) {
        String DATE_FORMAT = "yyyy-MM-dd";
        Date date = new Date(timestamp);
        String dateStr;
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
        dateStr = df.format(date);
        return dateStr;
    }

    /**
     * 将时间戳转为时分秒
     *
     * @param timestamp
     * @return
     */
    public static String timeStampToTimeString(long timestamp) {
        String DATE_FORMAT = "HH:mm:ss";
        Date date = new Date(timestamp);
        String dateStr;
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
        dateStr = df.format(date);
        return dateStr;
    }

    /**
     * 将格式yyyy-MM-dd HH:mm 转为时间戳
     *
     * @param time
     * @return
     */
    public static long timeStringTotimeStamp(String date, String time) {

        String dateTime = date + " " + time;
        String DATE_FORMAT = "yyyy-MM-dd HH:mm";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT);
        LocalDateTime parse = LocalDateTime.parse(dateTime, dtf);
        long timestamp = parse.toInstant(ZoneOffset.of("+8")).toEpochMilli();
        return timestamp;
    }
}
