package designpattern.factory.esayfactory;

/**
 * @author chencheng
 * @Description 简单工厂模式
 * 工厂可以根据需求生产不同季节的衣服
 * 但是违背了类设计的“开闭原则”，即对扩展开放，对修改封闭。每次新增加衣服品类无法扩展开放，而且都需要修改工厂类没有实现修改封闭
 * 同时，一个工厂既要生产短袖，要生产毛衣，又要生产衬衣，导致类的职责不明确，违背类设计的“单一职责”。
 * 可以把工厂进行分离，即生产短袖的工厂，生产衬衣的工厂，生产毛衣的工厂，客户需要的什么季节的衣服直接去对应的工厂拿就行
 * 抽象起来就是：①衣服抽象类Clothes不变；
 * ②具体衣服类不变，衬衣类Shirt，毛衣类Sweater，短袖类Cotta；
 * ③抽象工厂类Factory；抽象工厂都能做一件事，即生产衣服，不同工厂生产不同的衣服；
 * ④生产衬衣工厂类ShirtFactory；用来生产衬衣的工厂；
 * ⑤生产毛衣工厂类SweaterFactory；用来生产毛衣的工厂；
 * ⑥生产短袖工厂类CottaFactory；用来生产短袖的工厂；
 * ⑦顾客类Client；
 * 代码详见demo包下
 * @date 2019/10/15
 */
public class Factory {

    protected Clothes makeClothes(String type){
        Clothes clothes;
        switch (type) {
            case "spring":
                clothes = new Shirt();
                break;
            case "winter":
                clothes = new Sweater();
                break;
            case "summer":
                clothes = new Cotta();
                break;
            default:
                clothes = null;
                break;
        }
        return clothes;
    }

    class Shirt extends Clothes{
        public Shirt(){
            System.out.println("工厂制作一件衬衣");
        }
    }

    class Sweater extends Clothes{
        public Sweater(){
            System.out.println("工厂制作一件毛衣");
        }
    }

    class Cotta extends Clothes{
        public Cotta(){
            System.out.println("工厂制作一件短袖");
        }
    }

    /**
     * 客户端
     * @param args
     */
    public static void main(String[] args) {

        Factory factory = new Factory();
        factory.makeClothes("spring");
        factory.makeClothes("summer");
        factory.makeClothes("winter");
    }

}
